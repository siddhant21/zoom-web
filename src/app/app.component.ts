import { Component, OnInit, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

import { ZoomMtg } from '@zoomus/websdk';
import { ActivatedRoute } from '@angular/router';

ZoomMtg.setZoomJSLib('https://source.zoom.us/2.5.0/lib', '/av');

ZoomMtg.preLoadWasm();
ZoomMtg.prepareWebSDK();
// loads language files, also passes any error messages to the ui
ZoomMtg.i18n.load('en-US');
ZoomMtg.i18n.reload('en-US');

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  // setup your signature endpoint here: https://github.com/zoom/meetingsdk-sample-signature-node.js
  signatureEndpoint = 'http://localhost:4000';
  // This Sample App has been updated to use SDK App type credentials https://marketplace.zoom.us/docs/guides/build/sdk-app
  sdkKey = '21QeFnp6Pz4vRTHsQuN9h0Z0lY3qORiuVCi9';
  meetingNumber: string;
  role: number;
  leaveUrl = 'http://localhost:4200';
  userName = 'Angular';
  // userEmail = 'test@test.com';
  // passWord = 'password';
  // pass in the registrant's token if your meeting or webinar requires registration. More info here:
  // Meetings: https://marketplace.zoom.us/docs/sdk/native-sdks/web/client-view/meetings#join-registered
  // Webinars: https://marketplace.zoom.us/docs/sdk/native-sdks/web/client-view/webinars#join-registered
  registrantToken = '';
  apiAccessToken = '';
  teacherEmail = 'gaurav.gupta1@pw.live';

  constructor(
    public httpClient: HttpClient,
    @Inject(DOCUMENT) document,
    private activatedRoute: ActivatedRoute
  ) {
    // this.httpClient
    //   .get(`${this.signatureEndpoint}/token`)
    //   .subscribe((res: any) => (this.apiAccessToken = res.access_token));
  }

  ngOnInit() {
    const auth = btoa(
      'vTa7fdfnRCmne1Z2Q_0Erw:XnxBBiis5o8pNQ25SwrO5nnDS8bi6Ked'
    );
    console.log(this.activatedRoute.snapshot.queryParamMap);
    // this.meetingNumber = this.activatedRoute.snapshot.queryParamMap.get('mNum');
    this.role =
      this.activatedRoute.snapshot.paramMap.get('mode') === 'user' ? 0 : 1;
    this.userName = this.activatedRoute.snapshot.queryParamMap.get('userName');

    console.log(this.meetingNumber);
    console.log(this.role);
    console.log(this.userName);
    // if(this.role === 0) {
    // this.getLiveMeeting();
    // }
  }

  getLiveMeeting() {
    this.httpClient
      .get(
        `${this.signatureEndpoint}/list-meetings?userId=${this.teacherEmail}&type=live`
      )
      .subscribe((res: any) => {
        console.log(res);
        if(res.meetings?.length){
          this.meetingNumber = res.meetings[0].id;
          this.role = 0;
          this.getSignature();
        } else {
          window.alert('Teacher not live')
          this.role = 1;
        }
      });
  }

  getSignature() {
    this.httpClient
      .post(this.signatureEndpoint, {
        meetingNumber: this.meetingNumber,
        role: this.role,
      })
      .toPromise()
      .then((data: any) => {
        if (data.signature) {
          console.log(data.signature);
          this.startMeeting(data.signature);
        } else {
          console.log(data);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }

  createMeeting() {
    this.httpClient
      .get(
        `${this.signatureEndpoint}/list-meetings?userId=${this.teacherEmail}&type=all`
      )
      .subscribe((res: any) => {
        const meetings = res.meetings?.map(meeting => meeting?.id)
        this.httpClient
          .post(`${this.signatureEndpoint}/create-meeting`, {meetings})
          .subscribe((res: any) => (this.meetingNumber = res.id));
      });
  }

  startMeeting(signature) {
    document.getElementById('zmmtg-root').style.display = 'block';

    ZoomMtg.init({
      leaveUrl: this.leaveUrl,
      success: (success) => {
        console.log(success);
        console.log('signature', signature);
        console.log('meetingNumber', this.meetingNumber);
        console.log('userName', this.userName);
        console.log('sdkKey', this.sdkKey);
        ZoomMtg.join({
          signature: signature,
          meetingNumber: this.meetingNumber,
          userName: 'this.userName',
          sdkKey: this.sdkKey,
          // userEmail: this.userEmail,
          // passWord: this.passWord,
          // tk: this.registrantToken,
          success: (success) => {
            console.log(success);
          },
          error: (error) => {
            console.log(error);
          },
        });
      },
      error: (error) => {
        console.log(error);
      },
    });
  }
}
